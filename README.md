Migrate mercurial repositories from bitbucket to gitlab

# Prerequisites

Install [hg-fast-export](https://github.com/frej/fast-export).
You may want to follow the instructions of [Fast Export, from Mercurial to Git](https://medium.com/@anicolaspp/fast-export-from-mercurial-to-git-345a3fc79930#.t4ugor5i2).
You may have to `pip install pluginloader`.

Collect all the projects you want to migrate. Dump them into a file `repos.txt` that will look like this
```
<username>/<repo>
<username>/<other repo>
...
```
In case you have many repos, you can do it semi-automatically using [Import your project from Bitbucket Cloud to GitLab](https://docs.gitlab.com/ee/user/project/import/bitbucket.html#importing-your-bitbucket-repositories). What you do is [start importing from Bitbucket](https://gitlab.com/import/bitbucket/status). It redirects you to be authenticated by Atlassian, then you're presented a table of all your Bitbucket repositories. It lists your git repositories first with a button to import each. They are followed by mercurial repositories marked as "Incompatible project". Copy that part of the page and remove everything after the tab character.
```
pbpaste | cut -f-1 > repos.txt
```

# Run

Just run
```
./migrate <gitlab username>
```