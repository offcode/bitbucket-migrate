#!/bin/sh

gitlab_user=$1
test -z $gitlab_user && echo "provide your gitlab username." 1>&2 && exit 1

while IFS= read -r line; do
    echo "Processing: $line"
    IFS='/' read -r -a array <<< "$line"
    username="${array[0]}"
    repo="${array[1]}"
    cd hg-work
    hg clone ssh://hg@bitbucket.org/$username/$repo
    cd ..
    mkdir git-work/$repo
    cd git-work/$repo
    git init
    hg-fast-export.sh -r ../../hg-work/$repo
    git checkout 
    git remote add origin git@gitlab.com:$gitlab_user/$repo.git
    git push origin --all
    cd ../..
done < repos.txt